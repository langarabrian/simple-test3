// Initialize our Feathers client application
// with hooks and authentication.
const client = feathers();

const restClient = feathers.rest();

client.configure(restClient.fetch(window.fetch));

// vote screen
const thingHTML = `<main class="login container">
  <div class="row">
    <div class="col-12 col-6-tablet push-3-tablet text-center heading">
      <h1 class="font-100">Thing List</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-6-tablet push-3-tablet col-4-desktop push-4-desktop">
      <form class="form" id="add-thing">

          <input type="text" name="thing">

        <button type="submit" class="button-primary">
          Add thing
        </button>
      </form>
      <ul class="thinglist"></ul>
    </div>
  </div>
</main>`;

// Helper to safely escape HTML
const escape = str => str.replace(/&/g, '&amp;')
  .replace(/</g, '&lt;').replace(/>/g, '&gt;');

// Renders a thing to the page
const addThing = thing => {
  // The user that sent this message (added by the populate-user hook)
  const thinglist = document.querySelector('.thinglist');
  // Escape HTML to prevent XSS attacks
  const text = escape(thing.name);

  thinglist.innerHTML += `<li>${text}</li>`;
};

// Shows the thing page
const showThing = async () => {
  document.getElementById('app').innerHTML = thingHTML;

  // Find the latest 25 messages. They will come with the newest first
  const things = await client.service('things').find({
    query: {
      $sort: { createdAt: -1 },
      $limit: 25
    }
  });

  // We want to show the newest message last
  things.data.reverse().forEach(addThing);
};


const addEventListener = (selector, event, handler) => {
  document.addEventListener(event, async ev => {
    if (ev.target.closest(selector)) {
      handler(ev);
    }
  });
};


// "Add thing" form submission handler
addEventListener('#add-thing', 'submit', async ev => {
  // This is the vote text input field
  const input = document.querySelector('[name="thing"]');

  ev.preventDefault();

  // Create a new thing and then clear the input field
  await client.service('things').create({
    name: input.value
  });

  input.value = '';
});


// Listen to created events and add the new thing in real-time
client.service('things').on('created', addThing);

showThing();
