import app from '../../src/app';

describe('\'things\' service', () => {
  it('registered the service', () => {
    const service = app.service('things');
    expect(service).toBeTruthy();
  });

  it('creates a thing', async () => {
    const thing = await app.service('things').create( { name: 'banana' } );

    expect( thing.name ).toBe( 'banana' );
  });

});
